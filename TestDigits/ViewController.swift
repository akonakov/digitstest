//
//  ViewController.swift
//  TestDigits
//
//  Created by Aleksandr Konakov on 24/12/15.
//  Copyright © 2015 Aleksandr Konakov. All rights reserved.
//

import UIKit
import DigitsKit

class ViewController: UIViewController {


  @IBOutlet weak var logoutButton: UIBarButtonItem!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    let authButton = DGTAuthenticateButton(authenticationCompletion: { (session: DGTSession?, error: NSError?) in
      if (session != nil) {
        self.logoutButton.enabled = true
        // TODO: associate the session userID with your user model
        let message = "Phone number: \(session!.phoneNumber)"
        let alertController = UIAlertController(title: "You are logged in!", message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: .None))
        self.presentViewController(alertController, animated: true, completion: .None)
      } else {
        NSLog("Authentication error: %@", error!.localizedDescription)
      }
    })
    authButton.center = self.view.center
    self.view.addSubview(authButton)

  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func logoutPressed(sender: AnyObject) {
    Digits.sharedInstance().logOut()
    logoutButton.enabled = false
  }

}

